import React from 'react';
import { Heart, HeartFill, Trash, PencilSquare, CheckCircle } from 'react-bootstrap-icons';

class Message extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      value: ''
    };

    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleLikeClick = this.handleLikeClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleEnterClick = this.handleEnterClick.bind(this);
  }

  handleDeleteClick() {
    this.props.deleteMessage(this.props.m.id);
  }

  handleLikeClick() {
    this.props.likeMessage(this.props.m.id);
  }

  handleEditClick() {
    this.setState({
      ...this.state,
      value: this.props.m.text,
      edit: !this.state.edit
    });
  }

  handleChange(event) {
    this.setState({
      ...this.state,
      value: event.target.value
    });
  }

  handleEnterClick() {
    if (this.state.value !== '') {
      let newMessage = {};
      Object.assign(newMessage, this.props.m);
      newMessage.text = this.state.value;
      newMessage.editedAt = new Date().toISOString();

      this.props.editMessage(newMessage);
      this.setState({
        ...this.state,
        value: ''
      });
    }
  }

  render() {
    return (
      <div className="message">
        <div className="message-main">
          {this.props.m.avatar && <img src={this.props.m.avatar} />}
          <p>{this.props.m.text}</p>
        </div>
        <div className="message-secondary">
        <p style={{fontSize : '13.5px'}}>{new Date(this.props.m.createdAt).toLocaleString()}</p>
          {this.props.m.userId === '1' &&
            <Trash className="icon-button" onClick={this.handleDeleteClick} />}
          {this.props.m.userId === '1' &&
            <PencilSquare className="icon-button" onClick={this.handleEditClick} />}
          {this.props.m.userId !== '1' &&
            (this.props.m.isLike === true ? 
            <HeartFill className="icon-button" onClick={this.handleLikeClick} /> : 
            <Heart className="icon-button" onClick={this.handleLikeClick} />)}
          {this.state.edit &&
            <div className="edit-wrapper">
              <input type="text" className="edit-input" autoFocus placeholder="Messgae" value={this.state.value} onChange={this.handleChange} />
              <CheckCircle className="icon-button" onClick={this.handleEnterClick} />
            </div>}
        </div>
      </div>
    );
  }
}

export default Message;