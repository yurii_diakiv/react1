import React from 'react';

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      value: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleClick() {
    if (this.state.value !== '') {
      this.props.sendMessage(this.state.value);
      this.setState({ value: '' });
    }
  }

  render() {
    return (
      <div className="input-wrapper">
        <input type="text" className="message-input" autoFocus placeholder="Message" value={this.state.value} onChange={this.handleChange} />
        <button onClick={this.handleClick}>Send</button>
      </div>
    );
  }
}

export default MessageInput;