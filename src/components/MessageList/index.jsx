import React from 'react';
import Message from '../Message';

class MessageList extends React.Component {
  render() {
    return (
      <div className="message-list">
        { this.props.messages.map((m, index, array) =>
          <React.Fragment key={m.id}>
            <Message
              m={m}
              deleteMessage={this.props.deleteMessage}
              likeMessage={this.props.likeMessage}
              editMessage={this.props.editMessage}
            />
            {new Date(new Date(m.createdAt).getFullYear(), new Date(m.createdAt).getMonth(), new Date(m.createdAt).getDate()) <
              new Date(new Date(array[index + 1]?.createdAt).getFullYear(), new Date(array[index + 1]?.createdAt).getMonth(), new Date(array[index + 1]?.createdAt).getDate())
              &&
              <div className="divider">
                {new Date(array[index + 1].createdAt).toDateString()}
              </div>}
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default MessageList;