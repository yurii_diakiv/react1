import React from 'react';

class Spinner extends React.Component {
  render() {
    return (
      <div className="flex-centered height-fullscreen">
        <div className="loader"></div>
      </div>
    );
  }
}

export default Spinner;