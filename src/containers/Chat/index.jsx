import React from 'react';
import Header from '../../components/Header';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import Spinner from '../../components/Spinner';

class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [],
      newId: 1
    };

    this.sendMessage = this.sendMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.likeMessage = this.likeMessage.bind(this);
    this.editMessage = this.editMessage.bind(this);
  }

  sendMessage(message) {
    let newMessage = {
      id: '' + this.state.newId,
      userId: "1",
      avatar: '',
      user: "Me",
      text: message,
      createdAt: new Date().toISOString(),
      editedAt: ""
    }

    this.setState({
      messages: [...this.state.messages, newMessage],
      newId: this.state.newId + 1
    });
  }

  deleteMessage(id) {
    this.setState({
      ...this.state,
      messages: this.state.messages.filter(message => message.id !== id)
    });
  }

  likeMessage(id) {
    this.setState({
      ...this.state,
      messages: this.state.messages.map(m => {
        if (m.id == id)
          m.isLike = !m.isLike;
        return m;
      })
    });
  }

  editMessage(message) {
    this.setState({
      ...this.state,
      messages: this.state.messages.map(m => (m.id === message.id ? message : m))
    });
  }

  componentDidMount() {
    fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
      .then(response => response.json())
      .then(data => {
        let messages = data.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt))
          .map(m => {
            m['isLike'] = false;
            return m;
          });
        this.setState({
          messages: messages
        });
      });
  }

  render() {
    return (
      <>
        {
          this.state.messages.length !== 0 ?
            (
              <div className="chat">
                <Header messages={this.state.messages} />
                <MessageList
                  messages={this.state.messages}
                  deleteMessage={this.deleteMessage}
                  likeMessage={this.likeMessage}
                  editMessage={this.editMessage}
                />
                <MessageInput sendMessage={this.sendMessage} />
              </div>
            ) :
            <Spinner />
        }
      </>
    )
  }
}
export default Chat;